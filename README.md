ggplot2 routines of the IPK QG group
====================================

Overview
--------

This package currently contains the following functions:

  * Axis manipulations
    * hide axes(`hide_x`, `hide_y`)
    * rotate labels (`xlab_rot`, `ylab_rot`)
  
  * Improved logarithmic scales
    * Log scale with minor ticks (`scale_x_log10_mt`, `scale_y_log10_mt`)
    * Reverse log scales (`scale_*_log10_mt(..., rev = TRUE)`)
    * Pretty labelling of log scales, e.g. ($1\cdot10^-3$) with 
      `scale_*_log10_mt(..., labeller = log_labeller(...))` (default for
      `scale_*_log10_mt`, only needed for labeller options)
      
  * Moving window smoothing
    * `stat_movingwindow`
      

Installation
------------

Install via 

    devtools::install_bitbucket("mlell/ipkqggg")
